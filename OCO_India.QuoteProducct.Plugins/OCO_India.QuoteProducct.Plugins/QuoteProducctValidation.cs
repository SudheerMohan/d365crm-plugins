﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OCO_India.QuoteProducct.Plugins
{

    public class QuoteProducctValidation : IPlugin
    {
        public void Execute(IServiceProvider serviceProvider)
        {
            IPluginExecutionContext context = (IPluginExecutionContext)serviceProvider.GetService(typeof(IPluginExecutionContext));
            IOrganizationServiceFactory factory = (IOrganizationServiceFactory)serviceProvider.GetService(typeof(IOrganizationServiceFactory));
            IOrganizationService service = factory.CreateOrganizationService(context.UserId);
            ITracingService tracingService = (ITracingService)serviceProvider.GetService(typeof(ITracingService));
            tracingService.Trace("started....");

            if (context.MessageName == "Create")
            {
                Entity quote1 = null;
                try
                {
                    Entity QuoteProdentity = (Entity)context.InputParameters["Target"];
                    EntityCollection entcolconfig = getCnfiguration(service, tracingService);

                    Money DealerPrice = new Money(0);
                    Money dealermarkupfinal = new Money(0);

                    if (QuoteProdentity.LogicalName == "quotedetail")
                    {
                        tracingService.Trace("inside quotedetails");

                        if (QuoteProdentity.Attributes.Contains("quoteid"))
                        {
                            EntityReference quoteref = (EntityReference)QuoteProdentity.Attributes["quoteid"];
                            Entity quote = service.Retrieve(quoteref.LogicalName, quoteref.Id, new ColumnSet(true));
                            quote1 = quote;
                            tracingService.Trace("qouteref");

                            if (quote.Attributes.Contains("omnijj_excelimporttype") && quote.Attributes.Contains("omnijj_dealermarkuppercent"))
                            {
                                int excletypeOP = QuoteProdentity.GetAttributeValue<OptionSetValue>("omnijj_excelimporttype") != null ?
                                    QuoteProdentity.GetAttributeValue<OptionSetValue>("omnijj_excelimporttype").Value : 0;//(OptionSetValue)QuoteProdentity.Attributes["omnijj_excelimporttype"];
                                QuoteProdentity.Attributes["omnijj_dealermarkup"] = QuoteProdentity.Contains("omnijj_dealermarkup") ?
                                    quote.GetAttributeValue<decimal>("omnijj_dealermarkuppercent") : 0;// quote.Attributes["omnijj_dealermarkuppercent"];

                                if (excletypeOP == 500000000 && QuoteProdentity.Contains("omnijj_dealermarkup"))
                                {
                                    tracingService.Trace("50...");
                                    //tracingService.Trace(QuoteProdentity.Attributes["omnijj_dealermarkup"].ToString());
                                    dealermarkupfinal = new Money((Decimal)QuoteProdentity.Attributes["omnijj_dealermarkup"]);
                                    tracingService.Trace("58....");
                                    //  tracingService.Trace("43....");
                                    if (QuoteProdentity.Attributes.Contains("omnijj_hospitalprice"))
                                    {
                                        tracingService.Trace("62....");
                                        Money hp = (Money)QuoteProdentity.Attributes["omnijj_hospitalprice"];

                                        //var dt = Convert.ToDecimal(hp.Value) - (Convert.ToDecimal(hp.Value) / (1 + Convert.ToDecimal(dealermarkupfinal.Value)));
                                        var dt = ((Convert.ToDecimal(hp.Value) / (1 + (Convert.ToDecimal(dealermarkupfinal.Value) / 100))));
                                        DealerPrice = new Money(dt);
                                        tracingService.Trace("56...");

                                        QuoteProdentity.Attributes["omnijj_dealerprice"] = DealerPrice;

                                        tracingService.Trace("hp:" + Convert.ToDecimal(hp.Value));
                                        if (hp == null)
                                        {
                                            tracingService.Trace("End Customer Price(excl GST) can't be null.");
                                            throw new InvalidPluginExecutionException("End Customer Price(excl GST)  can't be null");

                                        }
                                    }
                                }
                                else if (excletypeOP == 500000001)
                                {
                                    if (QuoteProdentity.Attributes.Contains("omnijj_hospitalprice") == false)
                                    {
                                        tracingService.Trace("End Customer Price(excl GST) can't be null.");
                                        throw new InvalidPluginExecutionException("End Customer Price(excl GST) can't be null or zero.");
                                    }

                                    if (QuoteProdentity.Attributes.Contains("omnijj_dealerprice") == false)
                                    {
                                        tracingService.Trace("Price to the Dealer  can't be null or zero.");
                                        throw new InvalidPluginExecutionException("Price to the Dealer can't be null or zero.");
                                    }

                                    if (QuoteProdentity.Attributes.Contains("omnijj_hospitalprice"))
                                    {
                                        tracingService.Trace("95.....");
                                        Money hp = (Money)QuoteProdentity.Attributes["omnijj_hospitalprice"];

                                        //var dt = Convert.ToDecimal(hp.Value) - (Convert.ToDecimal(hp.Value) / (1 - Convert.ToDecimal(dealermarkupfinal.Value)));

                                        //DealerPrice = new Money(dt);


                                        //QuoteProdentity.Attributes["omnijj_dealerprice"] = DealerPrice;

                                        //tracingService.Trace("hp:" + Convert.ToDecimal(hp.Value));
                                        //if (hp == null || Convert.ToDecimal(hp.Value) == 0)
                                        //{
                                        //    tracingService.Trace("End Customer Price(excl GST) can't be null or zero.");
                                        //    throw new InvalidPluginExecutionException("End Customer Price(excl GST) can't be null or zero");

                                        //}

                                        tracingService.Trace("hp:" + Convert.ToDecimal(hp.Value));
                                        if (hp == null)
                                        {
                                            tracingService.Trace("End Customer Price(excl GST) can't be null.");
                                            throw new InvalidPluginExecutionException("End Customer Price(excl GST) can't be null");

                                        }

                                    }

                                    if (QuoteProdentity.Attributes.Contains("omnijj_dealerprice"))
                                    {

                                        Money dp = (Money)QuoteProdentity.Attributes["omnijj_dealerprice"];
                                        tracingService.Trace("dp :" + Convert.ToDecimal(dp.Value));
                                        //if (dp == null || Convert.ToDecimal(dp.Value) == 0)
                                        //{
                                        //    tracingService.Trace("Price to the Dealer can't be null or zero");
                                        //    throw new InvalidPluginExecutionException("Price to the Dealer can't be null or zero");
                                        //}
                                        if (dp == null)
                                        {
                                            tracingService.Trace("Price to the Dealer can't be null");
                                            throw new InvalidPluginExecutionException("Price to the Dealer can't be null");
                                        }
                                    }
                                    //if (qoute.Attributes.Contains("omnijj_orcper"))
                                    //{
                                    //    tracingService.Trace("omnijj_orc...." + qoute.Attributes["omnijj_orc"]);
                                    //    QuoteProdentity.Attributes["omnijj_orc"] = qoute.Attributes["omnijj_orcper"];
                                    //    tracingService.Trace("omnijj_orcper...." + qoute.Attributes["omnijj_orcper"]);
                                    //}
                                    //tracingService.Trace("42....");
                                    //if (qoute.Attributes.Contains("omnijj_todpercent"))
                                    //{
                                    //    //tracingService.Trace("omnijj_tod...." + qoute.Attributes["omnijj_tod"]);
                                    //    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    //    QuoteProdentity.Attributes["omnijj_tod"] = qoute.Attributes["omnijj_todpercent"];
                                    //}
                                }
                            }

                            if (quote.Attributes.Contains("omnijj_hospitalname") && QuoteProdentity.Attributes.Contains("productid"))
                            {
                                EntityReference hospital = quote.GetAttributeValue<EntityReference>("omnijj_hospitalname");
                                EntityReference product = QuoteProdentity.GetAttributeValue<EntityReference>("productid");

                                tracingService.Trace("calling GetPreviousApprovedQuoteProducts");

                                EntityCollection PrevQuoteProds = GetPreviousApprovedQuoteProducts(service, tracingService, hospital.Id.ToString(), product.Id.ToString());

                                tracingService.Trace("previous approved quote products : " + PrevQuoteProds.Entities.Count);
                                if (PrevQuoteProds.Entities.Count > 0)
                                {
                                    bool isFreeproduct = true;//is current product is free product.
                                    Money hsptPrice = QuoteProdentity.GetAttributeValue<Money>("omnijj_hospitalprice");

                                    tracingService.Trace("Current Hstpl price : " + hsptPrice.Value);

                                    if (hsptPrice.Value > 0)
                                        isFreeproduct = false;

                                    foreach (Entity QuoteProd in PrevQuoteProds.Entities)
                                    {
                                        Money PrevHsptPrice = QuoteProd.GetAttributeValue<Money>("omnijj_hospitalprice");

                                        tracingService.Trace("Previous approved quote product is price : " + PrevHsptPrice.Value);

                                        if (PrevHsptPrice.Value > 0)
                                        {
                                            tracingService.Trace("Previous approved quote product is priced product");

                                            decimal percentage = ((hsptPrice.Value - PrevHsptPrice.Value) / PrevHsptPrice.Value) * 100;


                                            QuoteProdentity.Attributes["omnijj_percentagepriceincrease"] = percentage;
                                            break;
                                        }
                                        else
                                        {
                                            tracingService.Trace("Previous approved quote product is Free product");

                                            if (isFreeproduct)
                                                QuoteProdentity.Attributes["omnijj_percentagepriceincrease"] = Convert.ToDecimal(0);
                                            else
                                                QuoteProdentity.Attributes["omnijj_remarks"] = "Previous Approved Product is Free Product";
                                        }
                                    }
                                }
                                else
                                {
                                    tracingService.Trace("No previous approved quote products found updating remarks ");
                                    QuoteProdentity.Attributes["omnijj_remarks"] = "No previous approved quote products found";
                                }
                            }

                            QuoteProdentity.Attributes["omnijj_orc"] = quote.GetAttributeValue<decimal>("omnijj_orcper");
                            tracingService.Trace($"ORCPer: {quote.GetAttributeValue<decimal>("omnijj_orcper")}");
                            QuoteProdentity.Attributes["omnijj_tod"] = quote.GetAttributeValue<decimal>("omnijj_todpercent");
                        }

                        if (QuoteProdentity.Attributes.Contains("omnijj_sku"))
                        {
                            string skucode = QuoteProdentity.Attributes["omnijj_sku"].ToString();

                            tracingService.Trace("135....");
                            ConditionExpression condition1 = new ConditionExpression();
                            condition1.AttributeName = "productnumber";
                            condition1.Operator = ConditionOperator.Equal;
                            condition1.Values.Add(skucode);

                            FilterExpression productfilter = new FilterExpression();
                            productfilter.Conditions.Add(condition1);

                            QueryExpression productQuery = new QueryExpression("product");

                            productQuery.ColumnSet = new ColumnSet(true);

                            productQuery.Criteria.AddFilter(productfilter);

                            EntityCollection entProduct = service.RetrieveMultiple(productQuery);

                            if (entProduct.Entities.Count != 0)
                            {
                                tracingService.Trace("156....");
                                Entity product = entProduct.Entities[0];

                                if (quote1 != null && QuoteProdentity.Attributes.Contains("omnijj_productsource")) // && QuoteProdentity.GetAttributeValue<OptionSetValue>("omnijj_productsource").Value== 500000000)
                                {
                                    EntityCollection QuoteProducts = getProductsforQuote(service, tracingService, quote1.Id.ToString(), product.Id.ToString());

                                    tracingService.Trace("product count : " + QuoteProducts.Entities.Count);
                                    if (QuoteProducts.Entities.Count >= 2)
                                    {
                                        throw new InvalidPluginExecutionException("Cannot add same product more than 2 times");
                                    }
                                    else
                                    {
                                        int pricecount = 0;
                                        bool freeproduct = false;

                                        foreach (Entity quoteprod in QuoteProducts.Entities)
                                        {
                                            Money hp = quoteprod.GetAttributeValue<Money>("omnijj_hospitalprice");
                                            Money Dp = quoteprod.GetAttributeValue<Money>("omnijj_dealerprice");

                                            if (hp.Value == 0 && Dp.Value == 0)
                                            {
                                                freeproduct = true;
                                            }
                                            else
                                            {
                                                pricecount++;
                                            }
                                        }

                                        tracingService.Trace("price count : " + pricecount);

                                        Money currentdp = (Money)QuoteProdentity.Attributes["omnijj_dealerprice"];
                                        Money currenthp = (Money)QuoteProdentity.Attributes["omnijj_hospitalprice"];
                                        tracingService.Trace("DP : " + currentdp.Value);
                                        tracingService.Trace("HP : " + currenthp.Value);

                                        if (pricecount >= 1 && (currentdp.Value > 0 || currenthp.Value > 0))
                                        {
                                            throw new InvalidPluginExecutionException("Cannot add more than one price product");
                                        }
                                        else if (freeproduct && (currentdp.Value == 0 && currenthp.Value == 0))
                                        {
                                            throw new InvalidPluginExecutionException("cannot add more than one free product");
                                        }
                                    }
                                }

                                if (product.Attributes.Contains("statuscode"))
                                {
                                    tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());

                                    int statuscode = ((OptionSetValue)product.Attributes["statuscode"]).Value;
                                    tracingService.Trace("statuscode...." + statuscode);
                                    if (statuscode != 1)
                                    {
                                        tracingService.Trace("Material is not active");
                                        throw new InvalidPluginExecutionException("Status of the Product is not active");
                                    }
                                }

                                // SKU type
                                if (product.Attributes.Contains("indskr_producttype"))
                                {
                                    int indskr_producttype = ((OptionSetValue)product.Attributes["indskr_producttype"]).Value;
                                    tracingService.Trace("indskr_producttype...." + indskr_producttype);
                                    if (indskr_producttype != 100000003)
                                    {
                                        tracingService.Trace("Selected Material is not SKU type");
                                        throw new InvalidPluginExecutionException("Selected Material is not SKU type");
                                    }
                                }
                                // Available for Upload
                                if (product.Attributes.Contains("omnijj_availableforupload"))
                                {
                                    int omnijj_availableforupload = ((OptionSetValue)product.Attributes["omnijj_availableforupload"]).Value;
                                    tracingService.Trace("indskr_producttype...." + omnijj_availableforupload);
                                    if (omnijj_availableforupload != 500000000)
                                    {
                                        tracingService.Trace("Product is not available for Upload");
                                        throw new InvalidPluginExecutionException("Product is not set to available for Upload");
                                    }
                                }
                                tracingService.Trace("defaultuomid....");
                                if (product.Attributes.Contains("defaultuomid") && QuoteProdentity.Attributes.Contains("uomid"))
                                {
                                    tracingService.Trace("176.....");
                                    EntityReference uomprod = (EntityReference)product.Attributes["defaultuomid"];
                                    EntityReference uompQtrod = (EntityReference)product.Attributes["defaultuomid"];
                                    tracingService.Trace(uomprod.Name);
                                    tracingService.Trace(uompQtrod.Name);
                                    if (uompQtrod.Id != uomprod.Id)
                                    {
                                        tracingService.Trace("UOM doesn't match, please use a valid UOM");
                                        throw new InvalidPluginExecutionException("UOM doesn't match, please use a valid UOM");

                                    }
                                }

                                //   tracingService.Trace("126....");
                                if (product.Attributes.Contains("omnijj_mrp") && QuoteProdentity.Attributes.Contains("omnijj_hospitalprice") && QuoteProdentity.Attributes.Contains("omnijj_dealerprice"))
                                {
                                    tracingService.Trace("190.....");
                                    Money hp = (Money)QuoteProdentity.Attributes["omnijj_hospitalprice"];
                                    Money dp = (Money)QuoteProdentity.Attributes["omnijj_dealerprice"];
                                    Money mrp = (Money)product.Attributes["omnijj_mrp"];

                                    tracingService.Trace("hp:" + Convert.ToDecimal(hp.Value));
                                    tracingService.Trace("dp:" + Convert.ToDecimal(dp.Value));
                                    tracingService.Trace("mrp:" + Convert.ToDecimal(mrp.Value));


                                    if (Convert.ToDecimal(mrp.Value) < Convert.ToDecimal(hp.Value))
                                    {
                                        tracingService.Trace("End Customer Price(excl GST) should not be greater than MRP");
                                        throw new InvalidPluginExecutionException("End Customer Price(excl GST) should not be greater than MRP. MRP price is " + Convert.ToDecimal(mrp.Value));
                                    }

                                    if (Convert.ToDecimal(mrp.Value) < Convert.ToDecimal(dp.Value))
                                    {
                                        tracingService.Trace("Price to the Dealer price should not be greater than MRP");
                                        throw new InvalidPluginExecutionException("Price to the Dealer should not be greater than MRP. MRP price is " + Convert.ToDecimal(mrp.Value));
                                    }
                                }

                                // tracingService.Trace("151....");
                                if (product.Attributes.Contains("omnijj_benchmarkprice") && QuoteProdentity.Attributes.Contains("omnijj_hospitalprice") && QuoteProdentity.Attributes.Contains("omnijj_dealerprice"))
                                {
                                    tracingService.Trace("215....");
                                    Money hp = (Money)QuoteProdentity.Attributes["omnijj_hospitalprice"];
                                    Money dp = (Money)QuoteProdentity.Attributes["omnijj_dealerprice"];
                                    Money benchmarkprice = (Money)product.Attributes["omnijj_benchmarkprice"];

                                    tracingService.Trace("hp:" + Convert.ToDecimal(hp.Value));
                                    tracingService.Trace("dp:" + Convert.ToDecimal(dp.Value));
                                    tracingService.Trace("benchmarkprice:" + Convert.ToDecimal(benchmarkprice.Value));


                                    if ((Convert.ToDecimal(benchmarkprice.Value) > Convert.ToDecimal(hp.Value)) || ((Convert.ToDecimal(benchmarkprice.Value) > Convert.ToDecimal(hp.Value))))
                                    {
                                        //tracingService.Trace("End Customer Price(excl GST) should not be less than Benchmark Price");
                                        //throw new InvalidPluginExecutionException("End Customer Price(excl GST) should not be less than Benchmark Price. Benchmark Price is " + Convert.ToDecimal(benchmarkprice.Value));
                                        QuoteProdentity.Attributes["omnijj_benchmarkpricealert"] = "Price should not be less than Benchmark Price. Benchmark Price is " + Convert.ToDecimal(benchmarkprice.Value);
                                        if (quote1 != null)
                                        {
                                            Entity QuoteUpdate = new Entity(quote1.LogicalName, quote1.Id);
                                            QuoteUpdate.Attributes["omnijj_bpcount"] = BPCount(service, tracingService, quote1.Id.ToString()) + 1;
                                            service.Update(QuoteUpdate);
                                        }
                                    }

                                    //if (Convert.ToDecimal(benchmarkprice.Value) > Convert.ToDecimal(dp.Value))
                                    //{
                                    ////    tracingService.Trace("Price to the Dealer should not be less than Benchmark Price");
                                    ////    throw new InvalidPluginExecutionException("Price to the Dealer should not be less than Benchmark Price. Benchmark Price is " + Convert.ToDecimal(benchmarkprice.Value));
                                    //    QuoteProdentity.Attributes["omnijj_benchmarkpricealert"] = "Price to the Dealer should not be less than Benchmark Price. Benchmark Price is " + Convert.ToDecimal(benchmarkprice.Value);
                                    //}
                                }

                                tracingService.Trace("180....");
                                if (product.Attributes.Contains("omnijj_gstrate"))
                                {
                                    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_gstrate"] = product.Attributes["omnijj_gstrate"];
                                }
                                //tracingService.Trace("Productid....");
                                //if (product.Attributes.Contains("name"))
                                //{
                                //    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                //    QuoteProdentity.Attributes["productid"] = product.Attributes["name"];
                                //}
                                tracingService.Trace("188....");
                                if (product.Attributes.Contains("omnijj_mrp"))
                                {
                                    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_mrp"] = product.Attributes["omnijj_mrp"];
                                }
                                if (product.Attributes.Contains("omnijj_mpgcode"))
                                {
                                    // tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_mpgcode"] = product.Attributes["omnijj_mpgcode"];
                                }
                                tracingService.Trace("194....");
                                if (product.Attributes.Contains("omnijj_benchmarkprice"))
                                {
                                    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_benchmarkprice"] = product.Attributes["omnijj_benchmarkprice"];
                                }

                                //if (product.Attributes.Contains("amount"))
                                //{
                                //    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                //    QuoteProdentity.Attributes["omnijj_standardhospitalprice"] = product.Attributes["amount"];
                                //    QuoteProdentity.Attributes["omnijj_standarddealerprice"] = product.Attributes["amount"];
                                //}

                                tracingService.Trace("208....");
                                if (product.Attributes.Contains("omnijj_addbackcost"))
                                {
                                    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_addbackcost"] = product.Attributes["omnijj_addbackcost"];
                                }
                                tracingService.Trace("274....");
                                if (product.Attributes.Contains("omnijj_uomdescription"))
                                {
                                    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_uomdescription"] = product.Attributes["omnijj_uomdescription"];
                                }
                                tracingService.Trace("215....");
                                if (product.Attributes.Contains("omnijj_addbackpersku2020"))
                                {
                                    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_addbackpersku2020"] = product.Attributes["omnijj_addbackpersku2020"];
                                }
                                tracingService.Trace("222....");
                                if (product.Attributes.Contains("standardcost"))
                                {
                                    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_cost2020"] = product.Attributes["standardcost"];
                                }

                                tracingService.Trace("228....");
                                if (product.Attributes.Contains("omnijj_mfgcost"))
                                {
                                    //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                    QuoteProdentity.Attributes["omnijj_mfgcost"] = product.Attributes["omnijj_mfgcost"];
                                }
                                tracingService.Trace("348....");


                                tracingService.Trace("product adding....");
                                QuoteProdentity.Attributes["productid"] = new EntityReference(product.LogicalName, product.Id);

                                if (product.Attributes.Contains("name"))
                                {
                                    EntityCollection entrodlvl = getProductLevel(service, tracingService, product.Attributes["name"].ToString());
                                    if (entrodlvl.Entities.Count != 0)
                                    {
                                        Entity prodctCol = entrodlvl.Entities[0];


                                        #region check L1 l2
                                        if (quote1 != null && quote1.Attributes.Contains("omnijj_salesperson"))
                                        {
                                            //EntityReference salesprsn = (EntityReference)quote1.Attributes["omnijj_salesperson"];
                                            //Guid BUID = getRquestorBU(service, tracingService, salesprsn.Id);
                                            //EntityCollection ProdBU = getProductBU(service, tracingService, BUID.ToString(), ((AliasedValue)prodctCol.Attributes["L1.productnumber"]).Value.ToString(), ((AliasedValue)prodctCol.Attributes["L2.productnumber"]).Value.ToString());
                                            EntityReference BU = (EntityReference)quote1.Attributes["omnijj_businessvertical"];
                                            EntityCollection ProdBU = getProductBU(service, tracingService, BU.Id.ToString(), ((AliasedValue)prodctCol.Attributes["L1.productnumber"]).Value.ToString(), ((AliasedValue)prodctCol.Attributes["L2.productnumber"]).Value.ToString());
                                            tracingService.Trace("402....");
                                            tracingService.Trace("Product BU count : " + ProdBU.Entities.Count);

                                            if (ProdBU.Entities.Count == 0)
                                            {
                                                throw new InvalidPluginExecutionException("Requested Material is not Valid for the selected Requestor BU");
                                            }
                                        }
                                        #endregion

                                        if (prodctCol.Attributes.Contains("L3.name"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l3name"] = ((AliasedValue)prodctCol.Attributes["L3.name"]).Value.ToString();
                                        }

                                        if (prodctCol.Attributes.Contains("L3.productnumber"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l3code"] = ((AliasedValue)prodctCol.Attributes["L3.productnumber"]).Value.ToString();
                                        }
                                        if (prodctCol.Attributes.Contains("L4.name"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l4name"] = ((AliasedValue)prodctCol.Attributes["L4.name"]).Value.ToString();
                                        }

                                        if (prodctCol.Attributes.Contains("L4.productnumber"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l4code"] = ((AliasedValue)prodctCol.Attributes["L4.productnumber"]).Value.ToString();
                                        }
                                        if (prodctCol.Attributes.Contains("L5.name"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l5description"] = ((AliasedValue)prodctCol.Attributes["L5.name"]).Value.ToString();
                                        }

                                        if (prodctCol.Attributes.Contains("L5.productnumber"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l5code"] = ((AliasedValue)prodctCol.Attributes["L5.productnumber"]).Value.ToString();
                                        }
                                        if (prodctCol.Attributes.Contains("L1.name"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l1decription"] = ((AliasedValue)prodctCol.Attributes["L1.name"]).Value.ToString();
                                        }

                                        if (prodctCol.Attributes.Contains("L1.productnumber"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l1code"] = ((AliasedValue)prodctCol.Attributes["L1.productnumber"]).Value.ToString();
                                        }
                                        if (prodctCol.Attributes.Contains("L2.name"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l2decription"] = ((AliasedValue)prodctCol.Attributes["L2.name"]).Value.ToString();
                                        }

                                        if (prodctCol.Attributes.Contains("L2.productnumber"))
                                        {
                                            QuoteProdentity.Attributes["omnijj_l2code"] = ((AliasedValue)prodctCol.Attributes["L2.productnumber"]).Value.ToString();
                                        }
                                        tracingService.Trace("245.... count is not 0, count is" + entrodlvl.Entities.Count);
                                    }
                                }

                                tracingService.Trace("244....");


                                tracingService.Trace("246....");
                                if (entcolconfig.Entities.Count != 0)
                                {
                                    tracingService.Trace("249....");
                                    Entity configent = entcolconfig.Entities[0];

                                    tracingService.Trace("252....");
                                    if (configent.Attributes.Contains("omnijj_dealerpricelistname") && configent.Attributes.Contains("omnijj_hospitalpricelistname"))
                                    {
                                        tracingService.Trace("256....");
                                        // EntityReference dealerprce = (EntityReference)configent.Attributes["omnijj_dealerpricelistname"];

                                        tracingService.Trace("258....");
                                        EntityCollection TandCTempEntities = gethospitalprice(service, tracingService, configent.Attributes["omnijj_dealerpricelistname"].ToString(), configent.Attributes["omnijj_hospitalpricelistname"].ToString(), skucode);


                                        tracingService.Trace("263....");
                                        if (TandCTempEntities.Entities.Count >= 2)
                                        {
                                            tracingService.Trace("362....");
                                            //  Entity plient = TandCTempEntities.Entities[0];
                                            if (TandCTempEntities.Entities[0].Attributes.Contains("amount"))
                                            {

                                                QuoteProdentity.Attributes["omnijj_standardhospitalprice"] = (Money)TandCTempEntities.Entities[0].Attributes["amount"];
                                            }
                                            else
                                            {
                                                throw new InvalidPluginExecutionException("Standard Hospital Price is not available in Pricelist");
                                            }
                                            if (TandCTempEntities.Entities[1].Attributes.Contains("amount"))
                                            {

                                                QuoteProdentity.Attributes["omnijj_standarddealerprice"] = (Money)TandCTempEntities.Entities[1].Attributes["amount"];
                                            }
                                            else
                                            {
                                                throw new InvalidPluginExecutionException("Standard Dealer Price is not available in Pricelist");
                                            }
                                        }
                                        else
                                        {
                                            tracingService.Trace("418....");
                                            if (TandCTempEntities.Entities.Count <= 1)
                                            {
                                                tracingService.Trace("420....");
                                                throw new InvalidPluginExecutionException("Pricelist is not available");

                                            }

                                        }

                                        if (product.Attributes.Contains("defaultuomid"))
                                        {
                                            //tracingService.Trace("statuscode...." + product.Attributes["statuscode"].ToString());
                                            QuoteProdentity.Attributes["uomid"] = product.Attributes["defaultuomid"];
                                        }




                                    }



                                    tracingService.Trace("262....");

                                }


                            }
                            else
                            {
                                tracingService.Trace("SKU/Product code is not valid");
                                throw new InvalidPluginExecutionException("SKU/Product code is not valid");

                            }
                        }

                        if (quote1 != null && QuoteProdentity.Attributes.Contains("omnijj_productsource") && QuoteProdentity.GetAttributeValue<OptionSetValue>("omnijj_productsource").Value == 500000000)
                        {
                            tracingService.Trace("Entity:" + quote1.LogicalName + " Id: " + quote1.Id);
                            Entity QuoteExcelUpdate = new Entity(quote1.LogicalName, quote1.Id);
                            QuoteExcelUpdate.Attributes["omnijj_countfromexcel"] = ExcelImportCount(service, tracingService, quote1.Id.ToString()) + 1;
                            tracingService.Trace("Updating Count From Excel");
                            service.Update(QuoteExcelUpdate);
                        }
                    }
                }
                catch (Exception ex)
                {
                    tracingService.Trace("Validation Error:- " + ex.Message + "StackTrace:\n\n" + ex.StackTrace);
                    throw new InvalidPluginExecutionException("Validation Error: " + ex.Message);

                }
            }

            else if (context.MessageName == "Delete")
            {
                Entity QPPreImage = (Entity)context.PreEntityImages["PreImage"];

                if (QPPreImage.Attributes.Contains("quoteid"))
                {
                    EntityReference quote1 = QPPreImage.GetAttributeValue<EntityReference>("quoteid");

                    Entity QuoteUpdate = new Entity(quote1.LogicalName, quote1.Id);
                    QuoteUpdate.Attributes["omnijj_bpcount"] = BPCount(service, tracingService, quote1.Id.ToString());
                    QuoteUpdate.Attributes["omnijj_numberofproducts"] = Convert.ToDecimal(GetQPCount(service, tracingService, quote1.Id.ToString()));
                    QuoteUpdate.Attributes["omnijj_countfromexcel"] = ExcelImportCount(service, tracingService, quote1.Id.ToString());
                    service.Update(QuoteUpdate);

                }
            }
        }


        public EntityCollection getCnfiguration(IOrganizationService service, ITracingService tracingService)
        {
            QueryExpression configQuery = new QueryExpression("omnijj_configuration");
            configQuery.ColumnSet = new ColumnSet(true);
            tracingService.Trace("297....");
            EntityCollection entconfig = service.RetrieveMultiple(configQuery);
            tracingService.Trace("299....");
            return entconfig;

            //  return new EntityCollection();
        }

        public EntityCollection gethospitalprice(IOrganizationService service, ITracingService tracingService, string dpname, string hpname, string skucode)
        {
            tracingService.Trace("305....");
            tracingService.Trace("plname...." + hpname);
            tracingService.Trace("dplname...." + dpname);
            tracingService.Trace("skucode...." + skucode);


            var fetchingTC = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'> 
                                  <entity name='productpricelevel'> 
                                    <attribute name='productid' /> 
                                    <attribute name='uomid' /> 
                                    <attribute name='productpricelevelid' /> 
                                    <attribute name='amount' /> 
                                    <attribute name='productnumber' /> 
                                    <order attribute='productid' descending='false' /> 
                                    <filter type='and'> 
                                      <condition attribute='productnumber' operator='eq' value='" + skucode + @"' /> 
                                    </filter> 
                                    <link-entity name='pricelevel' from='pricelevelid' to='pricelevelid' link-type='inner' alias='ac'> 
                                      <attribute name='name' /> 
                                      <filter type='and'> 
                                        <filter type='or'> 
                                          <condition attribute='name' operator='eq' value='" + hpname + @"' /> 
                                          <condition attribute='name' operator='eq' value='" + dpname + @"' /> 
                                        </filter> 
                                      </filter> 
                                    </link-entity> 
                                  </entity> 
                                </fetch>";

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            EntityCollection TandCTempEntities = ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;

            return TandCTempEntities;

            //  return new EntityCollection();
        }

        public EntityCollection getProductLevel(IOrganizationService service, ITracingService tracingService, string productname)
        {
            tracingService.Trace("370....");


            var fetchingTC = @"<fetch top='50' >
                                      <entity name='product' >
                                        <attribute name='name' />
                                        <filter>
                                          <condition attribute='name' operator='like' value='" + productname + @"' />
                                        </filter>
                                        <link-entity name='product' from='productid' to='parentproductid' alias='L5' >
                                          <attribute name='name' />
                                          <attribute name='productnumber' />
                                          <link-entity name='product' from='productid' to='parentproductid' alias='L4' >
                                            <attribute name='name' />
                                            <attribute name='productnumber' />
                                            <link-entity name='product' from='productid' to='parentproductid' alias='L3' >
                                              <attribute name='name' />
                                              <attribute name='productnumber' />
                                              <link-entity name='product' from='productid' to='parentproductid' alias='L2' >
                                                <attribute name='name' />
                                                <attribute name='productnumber' />
                                                <link-entity name='product' from='productid' to='parentproductid' alias='L1' >
                                                  <attribute name='name' />
                                                  <attribute name='productnumber' />
                                                  </link-entity>
                                              </link-entity>
                                            </link-entity>
                                          </link-entity>
                                        </link-entity>
                                      </entity>
                                    </fetch>";

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            EntityCollection TandCTempEntities = ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;

            return TandCTempEntities;

            //  return new EntityCollection();
        }

        public Guid getRquestorBU(IOrganizationService service, ITracingService tracingService, Guid requestorId)
        {
            tracingService.Trace("592....");

            Guid BUid = Guid.Empty;

            var fetchingTC = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                  <entity name='systemuser'>
                                    <attribute name='fullname' />
                                    <attribute name='businessunitid' />
                                    <order attribute='fullname' descending='false' />
                                    <filter type='and'>
                                      <condition attribute='systemuserid' operator='eq' uitype='systemuser' value='" + requestorId + @"' />
                                    </filter>
                                  </entity>
                                </fetch>";

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            EntityCollection UserEntities = ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;
            if (UserEntities.Entities.Count != 0)
            {
                EntityReference bu = (EntityReference)UserEntities.Entities[0].Attributes["businessunitid"];
                BUid = bu.Id;
            }

            return BUid;
        }

        public EntityCollection getProductsforQuote(IOrganizationService service, ITracingService tracingService, string Quoteid, string productid)
        {
            var fetchingTC = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                  <entity name='quotedetail'>
                                    <attribute name='omnijj_dealerprice' />
                                    <attribute name='omnijj_hospitalprice' />
                                    <order attribute='productid' descending='false' />
                                    <filter type='and'>
                                      <condition attribute='quoteid' operator='eq' uiname='' uitype='quote' value='" + Quoteid + @"' />
                                      <condition attribute='productid' operator='eq' uiname='' uitype='product' value='" + productid + @"' />
                                    </filter>
                                  </entity>
                                </fetch>";

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            EntityCollection TandCTempEntities = ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;

            return TandCTempEntities;

        }

        public EntityCollection getProductBU(IOrganizationService service, ITracingService tracingService, string requestorBU, string l1, string l2)
        {
            tracingService.Trace("626....");
            tracingService.Trace("requestorBU : " + requestorBU);
            tracingService.Trace("l1 : " + l1);
            tracingService.Trace("l2 : " + l2);
            // Guid BUid = Guid.Empty;

            var fetchingTC = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                            "  <entity name='omnijj_prodbu'>" +
                            "    <attribute name='omnijj_prodbuid' />" +
                            "    <order attribute='omnijj_producthierarchy' descending='false' />" +
                            "    <filter type='and'>" +
                            "      <condition attribute='statecode' operator='eq' value='0' />" +
                            "      <condition attribute='omnijj_businessunit' operator='eq' uiname='' uitype='businessunit' value='" + requestorBU + "' />" +
                            "      <condition attribute='omnijj_l1' operator='eq' value='" + WebUtility.HtmlEncode(l1) + "' />" +
                            "      <condition attribute='omnijj_l2' operator='eq' value='" + WebUtility.HtmlEncode(l2) + "' />" +
                            "<filter type='or'>" +
                            "<condition attribute='omnijj_l3' operator='not-null'/>" +
                            "<condition attribute='omnijj_l3' operator='null'/>" +
                            "</filter>" +
                                "    </filter>" +
                            "  </entity>" +
                            "</fetch>";

            tracingService.Trace("fetch : " + fetchingTC);

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            return ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;

        }

        public EntityCollection GetPreviousApprovedQuoteProducts(IOrganizationService service, ITracingService tracingService, string Hospitalid, string productid)
        {
            tracingService.Trace("Checking Previous Quotations");

            var fetchingTC = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false' count='2'>
                                  <entity name='quotedetail'>
                                    <attribute name='omnijj_dealerprice' />
                                    <attribute name='omnijj_hospitalprice' />
                                    <attribute name='productid' />
                                    <attribute name='omnijj_sku' />
                                    <attribute name='quotedetailid' />
                                    <order attribute='modifiedon' descending='false' />
                                    <filter type='and'>
                                      <condition attribute='productid' operator='eq' uiname='' uitype='product' value='{" + productid + @"}' />
                                    </filter>
                                    <link-entity name='quote' from='quoteid' to='quoteid' link-type='inner' alias='ab'>
                                      <filter type='and'>
                                        <condition attribute='omnijj_hospitalname' operator='eq' uiname='' uitype='account' value='{" + Hospitalid + @"}' />
                                        <condition attribute='statuscode' operator='eq' value='4' />
                                      </filter>
                                    </link-entity>
                                  </entity>
                                </fetch>";

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            EntityCollection Quoteproducts = ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;

            return Quoteproducts;

        }

        public int GetQPCount(IOrganizationService service, ITracingService tracingService, string Quoteid)
        {
            tracingService.Trace("782....");

            var fetchingTC = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                  <entity name='quotedetail'>
                                    <attribute name='quotedetailid' />
                                    <order attribute='productid' descending='false' />
                                    <filter type='and'>
                                      <condition attribute='quoteid' operator='eq' uiname='' uitype='quote' value='" + Quoteid + @"' />
                                    </filter>
                                  </entity>
                                </fetch>";

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            EntityCollection TandCTempEntities = ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;

            return TandCTempEntities.Entities.Count;

        }

        public int BPCount(IOrganizationService service, ITracingService tracingService, string Quoteid)
        {
            tracingService.Trace("782....");
            
            var fetchingTC = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                                  <entity name='quotedetail'>
                                    <attribute name='quotedetailid' />
                                    <order attribute='productid' descending='false' />
                                    <filter type='and'>
                                      <condition attribute='quoteid' operator='eq' uiname='' uitype='quote' value='" + Quoteid + @"' />
                                      <condition attribute='omnijj_benchmarkpricealert' operator='not-null' />
                                    </filter>
                                  </entity>
                                </fetch>";

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            EntityCollection TandCTempEntities = ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;

            return TandCTempEntities.Entities.Count;

        }

        public int ExcelImportCount(IOrganizationService service, ITracingService tracingService, string Quoteid)
        {
            var fetchingTC = @"<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>
                              <entity name='quotedetail'>
                                <attribute name='quotedetailid' />
                                <order attribute='productid' descending='false' />
                                <filter type='and'>
                                  <condition attribute='quoteid' operator='eq' uiname='' uitype='quote' value='" + Quoteid + @"' />
                                  <condition attribute='omnijj_productsource' operator='eq' value='500000000' />
                                </filter>
                              </entity>
                            </fetch>";

            var TCLlistReq = new RetrieveMultipleRequest()
            {
                Query = new FetchExpression(fetchingTC)
            };
            //Get all 
            EntityCollection TandCTempEntities = ((RetrieveMultipleResponse)service.Execute(TCLlistReq)).EntityCollection;

            return TandCTempEntities.Entities.Count;
        }

    }

}