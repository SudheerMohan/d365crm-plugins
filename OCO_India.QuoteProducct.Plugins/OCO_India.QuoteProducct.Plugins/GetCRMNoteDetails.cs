﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Sdk.Workflow;

namespace OCO_India.QuoteProducct.Plugins
{
    public class GetCRMNoteDetails : CodeActivity
    {
        [RequiredArgument]
        [Input("File Name")]
        public InArgument<string> FileName { get; set; }

        [RequiredArgument]
        [Output("MIME Type")]
        public OutArgument<string> MIMEType { get; set; }

        [RequiredArgument]
        [Output("Note Body")]
        public OutArgument<string> NoteBody { get; set; }

        protected override void Execute(CodeActivityContext context)
        {
            ITracingService tracing = context.GetExtension<ITracingService>();

            try
            {
                string fileName = FileName.Get(context);
                if (string.IsNullOrEmpty(fileName))
                    return;

                var organizationServiceFactory = context.GetExtension<IOrganizationServiceFactory>();
                var service = organizationServiceFactory.CreateOrganizationService(null);
                tracing.Trace("Got the service");
                Entity noteBody = GetNoteBody(fileName, service);
                
                string mimetype = (string)noteBody.GetAttributeValue<string>("mimetype");
                tracing.Trace("mimetype : "+ mimetype);
                string documentbody = (string)noteBody.GetAttributeValue<string>("documentbody");
                //tracing.Trace("documentbody : " + documentbody);

                MIMEType.Set(context, mimetype);
                NoteBody.Set(context, documentbody);
                
            }
            catch (Exception ex)
            {
                throw new InvalidPluginExecutionException(ex.Message);
            }
        }

        public Entity GetNoteBody(string fileName,IOrganizationService service)
        {
            var fetchXml = "<fetch version='1.0' output-format='xml-platform' mapping='logical' distinct='false'>" +
                        "  <entity name='annotation'>" +
                        "    <attribute name='subject' />" +
                        "    <attribute name='documentbody' />" +
                        "    <attribute name='mimetype' />" +
                        "    <order attribute='subject' descending='false' />" +
                        "    <filter type='and'>" +
                        "      <condition attribute='filename' operator='not-null' />" +
                        "      <condition attribute='subject' operator='eq' value='" + fileName + "' />" +
                        "    </filter>" +
                        "    <link-entity name='omnijj_configuration' from='omnijj_configurationid' to='objectid' link-type='inner' alias='ad' />" +
                        "  </entity>" +
                        "</fetch>";
            var results = service.RetrieveMultiple(new FetchExpression(fetchXml));

            return results.Entities[0];

        }
    }
}

